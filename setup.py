from os.path import join, dirname

from setuptools import setup


setup(
    name='rebranch_shortcuts',
    version=0.2,
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    author='',
    url='https://bitbucket.org/rebranch/rebranch_shortcuts',
)