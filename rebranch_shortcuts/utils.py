#coding=utf8
from os import environ


def safe_cast(value, type, default=None):
    """
    Производит безопасное приведение value к типу type, в случае неудачи возвращает значение default, либо type()
    """
    try:
        return type(value)
    except (TypeError, ValueError):
        if default is None:
            return type()
        else:
            return default


class EnvironmentBasedValue(object):
    name = None
    value = None

    def __init__(self, name, default=None):
        self.name = name
        self.value = environ.get(name, default)

    def __get__(self, instance, owner):
        return self.value