from django.shortcuts import _get_queryset


def get_object_or_none(self, *args, **kwargs):
    queryset = _get_queryset(self)
    try:
        return queryset.get(*args, **kwargs)
    except queryset.model.DoesNotExist:
        return None


__all__ = [u'get_object_or_none']