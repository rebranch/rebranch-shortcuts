import json
from django.http.response import HttpResponse


class APIStatusMixin(object):
    class response_status(object):
        success = u'success'
        fail = u'fail'


class JSONPResponseMixin(APIStatusMixin):
    def render_to_jsonp_response(self, status, data, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        if not data:
            data = {}
        response = {
            u'status': status,
            u'data': data
        }
        return HttpResponse(
            self.convert_context_to_jsonp(response),
            **response_kwargs
        )

    def convert_context_to_jsonp(self, context):
        """Convert the context dictionary into a JSON object"""
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        response = u'{callback}({json})'.format(callback=self.request.GET.get(u'callback', u''),
                                                json=json.dumps(context))
        return response


class JSONResponseMixin(APIStatusMixin):
    """
    A mixin that can be used to render a JSON response.
    """

    def render_to_json_response(self, status, data=None, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        if not data:
            data = {}
        response = {
            u'status': status,
            u'data': data
        }
        return HttpResponse(
            self.convert_context_to_json(response),
            content_type='application/json',
            **response_kwargs
        )

    def convert_context_to_json(self, context):
        """Convert the context dictionary into a JSON object"""
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return json.dumps(context)


__all__ = [u'JSONPResponseMixin', u'JSONResponseMixin']
